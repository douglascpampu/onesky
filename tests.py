import unittest
from main import server, simulation


class TestServer(unittest.TestCase):

    def test_allocateUser(self):
        s = server()

        status = s.allocateUser('test1')

        self.assertTrue(status)

        users = s.getUsers()

        self.assertTrue('test1' in users.keys())
        self.assertEqual(users['test1'], 5)

    def test_cannotAllocateUser(self):
        s = server(uMax=1)

        s.allocateUser('test1')

        status = s.allocateUser('test2')

        self.assertFalse(status)
        users = s.getUsers()

        self.assertTrue('test2' not in users.keys())

    def test_runServer(self):
        s = server()

        s.allocateUser('test1')

        status = s.runServer()
        self.assertTrue(status)

        users = s.getUsers()
        self.assertEqual(users['test1'], 4)

        self.assertEqual(s.getBill(), 1)

    def test_runServerUntilProcessesAreDone(self):
        s = server()

        s.allocateUser('test1', Ttask=1)

        status = s.runServer()
        self.assertTrue(status)

        status = s.runServer()
        self.assertFalse(status)

        users = s.getUsers()
        self.assertEqual(len(users), 0)

        self.assertEqual(s.getBill(), 1)


class TestSimmulation(unittest.TestCase):

    def test_runOneUser(self):
        s = simulation([1])

        status = s.runSimulation()

        self.assertEqual(len(status), 5)

        self.assertEqual(s.getBill(), 5)

    def test_startTwoUsersSimultaneosly(self):
        s = simulation([2])

        status = s.runSimulation()

        self.assertEqual(len(status), 5)

        self.assertEqual(s.getBill(), 5)

    def test_startTwoUsersOneTickApart(self):
        s = simulation([1, 1])

        status = s.runSimulation()

        self.assertEqual(len(status), 6)

        self.assertEqual(s.getBill(), 6)

    def test_waitfiveTicksToStart(self):
        s = simulation([0, 0, 0, 0, 0, 1])

        status = s.runSimulation()

        self.assertEqual(len(status), 10)

        self.assertEqual(s.getBill(), 5)

    def test_runEmailExample(self):
        s = simulation([1, 3, 0, 1, 0, 1], uMax=2, Ttask=4)

        status = s.runSimulation()

        self.assertEqual(len(status), 9)

        self.assertEqual(s.getBill(), 15)


if __name__ == '__main__':
    unittest.main()