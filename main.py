import sys

class server:

    def __init__(self, uMax = 10):
        self.__users = {}
        self.__uMax = uMax
        self.__totalBill = 0

    def isServerAvaliable(self):
        return len(self.__users) < self.__uMax

    def allocateUser(self, userId, Ttask = 5):
        if len(self.__users.keys()) < self.__uMax:
            self.__users[userId] = Ttask
            return True
        else:
            return False

    def getUsers(self):
        return self.__users

    def __removeUser(self, userId):
        del self.__users[userId]

    def getBill(self):
        return self.__totalBill

    def runServer(self):
        if len(self.__users.keys()) > 0:
            usersToRemove = []
            for user in self.__users.keys():
                self.__users[user] -= 1
                if self.__users[user] == 0:
                    usersToRemove.append(user)
            for user in usersToRemove:
                self.__removeUser(user)
            self.__totalBill += 1
            return True
        else:
            return False

    def getMaxTime(self):
        return max(self.__users.values())


class simulation:

    def __init__(self, usersTimeline=[], uMax=10, Ttask=5):
        self.__usersTimeline = usersTimeline
        self.__servers = []
        self.__totalBill = 0
        self.uMax=uMax
        self.Ttask=Ttask
        pass

    def __findOrCreateServerForUser(self, userId):
        if len(self.__servers):
            for s in self.__servers:
                if s.isServerAvaliable():
                    return s.allocateUser(userId, Ttask=self.Ttask)

        s = server(uMax=self.uMax)
        s.allocateUser(userId, Ttask=self.Ttask)
        self.__servers.append(s)
        return True

    def __removeServer(self):
        emptyServer = (next((serverIndex for serverIndex in range(len(self.__servers)) if
                             len(self.__servers[serverIndex].getUsers()) == 0), -1))
        if emptyServer != -1:
            self.__totalBill = self.__totalBill + self.__servers[emptyServer].getBill()
            del self.__servers[emptyServer]
            return True
        return False

    def __clearServers(self):
        status = True
        while status:
            status = self.__removeServer()

    def __sortServersByOccupation(self, s):
        return s.getMaxTime()

    def runSimulation(self):

        tickStatus = []
        nextUserId = 0
        while len(self.__usersTimeline) or len(self.__servers):
            if len(self.__usersTimeline):
                for user in range(self.__usersTimeline[0]):
                    self.__findOrCreateServerForUser(nextUserId)
                    nextUserId += 1

                del self.__usersTimeline[0]

            tick = []
            for serverIndex in range(len(self.__servers)):
                tick.append({'server ': serverIndex, 'users': len(self.__servers[serverIndex].getUsers())})
                self.__servers[serverIndex].runServer()
            self.__clearServers()
            self.__servers.sort(key=self.__sortServersByOccupation, reverse=True)
            tickStatus.append(tick)

        return tickStatus
        pass

    def getBill(self):
        return self.__totalBill


def readFile(fileName):
    f = open(fileName, "r")
    fileData = f.read()

    return fileData.split('\n')


if __name__ == '__main__':
    inputArray = readFile(sys.argv[1])
    usersArray = []
    for line in inputArray:
        if line:
            usersArray.append(int(line))

    s = simulation(usersArray)

    simulationResult = s.runSimulation()
    totalBill = s.getBill()

    with open('output.txt', 'w') as out:
        fileData = ''
        for line in simulationResult:
            users = []
            for server in line:
                users.append(server['users'])
            fileData += str(users) + '\n'

        fileData += 'Total Bill: ' + str(totalBill)

        out.write(fileData)